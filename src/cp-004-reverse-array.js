function reverseArray(array) {
  let result = [];
  for (let i = array.length - 1; i >= 0; i--) {
    result.push(array[i]);
  }
  return result;
}
let arr0 = [3, 4, 5, 6];
let arr1 = reverseArray(arr0);
console.log(arr0 == arr1);

function reverseArrayInPlace(array) {
  function swap(array, i, j) {
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
    return array;
  }
  let lenOriginal = array.length;
  for (let w = 0; w < array.length; w++) {
    for (let i = 0; i < lenOriginal; i++) {
      swap(array, i, i + 1);
    }
    lenOriginal--;
  }
  return array;
}
arr0 = [3, 4, 5, 6];
arr1 = reverseArrayInPlace(arr0);
console.log(arr0 == arr1);
