class Vec {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  plus(vector) {
    const result = new Vec(0, 0);
    result.x = this.x + vector.x;
    result.y = this.y + vector.y;
    return result;
  }

  minus(vector) {
    const result = new Vec(0, 0);
    result.x = this.x - vector.x;
    result.y = this.y - vector.y;
    return result;
  }

  get length() {
    return Math.sqrt(this.x ** 2 + this.y ** 2);
  }
}

v1 = new Vec(3, 4);
v2 = new Vec(5, 6);
console.log(v1.plus(v2));
console.log(v1.minus(v2));
console.log(v1.length);

console.log(new Vec(1, 2).plus(new Vec(2, 3)));
// → Vec{x: 3, y: 5}
console.log(new Vec(1, 2).minus(new Vec(2, 3)));
// → Vec{x: -1, y: -1}
console.log(new Vec(3, 4).length);
// → 5
