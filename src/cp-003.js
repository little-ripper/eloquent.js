const minimum = function (a, b) {
  if (a < b) {
    return a;
  }
  return b;
};

console.log(minimum(5, 29));
console.log(minimum(45, 29));
