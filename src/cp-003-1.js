const isEven = function (N) {
  if (N <= 0) {
    return 0;
  } else if (N == 1) {
    return 1;
  } else {
    return isEven(N - 2);
  }
};

console.log(isEven(50));
console.log(isEven(75));
console.log(isEven(-1));
