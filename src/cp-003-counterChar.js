const countBs = function (str, char) {
  let counter = 0;
  for (let i = 0; i < str.length; i++) {
    if (str[i] == char) {
      counter++;
    }
  }
  return counter;
};

console.log(countBs("Hello BeBBe", "B"));
console.log(countBs("Luigi is moniello", "i"));
console.log(countBs("oooooooooooooooooooooooooooooooooooooooooooooooooo", "o")); // 50 o

const counterRec = function (str, char) {
  const counterChar = function (str, counter) {
    if (str.length === 0) {
      return counter;
    } else if (str[0] == char) {
      counter++;
    }
    return counterChar(str.slice(1, str.length), counter);
  };
  return counterChar(str, 0);
};

console.log(counterRec("Hello", "l"));
console.log(counterRec("Luigi is moniello", "i"));
console.log(counterRec("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK", "K")); // 34 K
