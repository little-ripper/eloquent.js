function arrayToList(array) {
  // given an array of integer
  const last = array.length - 1;
  let list = { value: array[last], rest: null };
  for (let i = last - 1; i >= 0; i--) {
    list = { value: array[i], rest: list };
  }
  return list;
}

array0 = [1, 2, 3];
list0 = arrayToList(array0);
console.log(list0);

function listToArray(list) {
  // given a list return an array
  let array = [];
  while (list.rest) {
    array.push(list.value);
    list = list.rest;
  }
  array.push(list.value);
  return array;
}

array1 = listToArray(list0);
console.log(array1);

function prepend(element, list) {
  // create a new list
  return { value: element, rest: list };
}

list1 = prepend(10, list0);
console.log(list1);
console.log("-----");

function nth(number, list) {
  function find(counter, list) {
    if (!list.rest) {
      return undefined;
    } else if (counter === number) {
      return list;
    } else {
      counter++;
      return find(counter, list.rest);
    }
  }
  return find(0, list);
}

list2 = nth(2, list1);
console.log(list2);
console.log("-----");

list3 = nth(12, list1);
console.log(list3);
