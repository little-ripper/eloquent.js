class MultiplicatorUnitFailure extends Error {}

function primitiveMultiply(a, b) {
  let result;
  result = a * b;
  if (isNaN(result)) {
    throw new Error(`Error`);
  }
  return result;
}
/* console.log(primitiveMultiply(3, 2)); */
/* console.log(primitiveMultiply(3, "hello")); */

const simu = [
  wrapper(primitiveMultiply(3, 2)),
  wrapper(primitiveMultiply(3, 2)),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
  wrapper(primitiveMultiply(3, "hello")),
];

function wrapper(func) {
  for (;;) {
    try {
      let result = func;
      console.log("Multiplication OK");
      break;
    } catch (e) {
      if (e instanceof MultiplicatorUnitFailure) {
        console.log("Multiplication failed!");
      } else {
        throw e;
      }
    }
  }
}
/* simu.map((f) => wrapper(f)); */
