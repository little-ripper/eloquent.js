// let total = 0,
//     count = 1;
// while (count <= 10) {
//     total += count;
//     count += 1;
// }
// console.log(total);

// precedence
console.log(true || true && false); // true
console.log(1 || 1 - 1); // 1
console.log(-1 + 1 && 1); // 0
console.log(1 + 0 && 1); // 1
console.log(3 + 1 && 1); // 1
console.log(1 + 1 == 2 && 10 * 10 > 50); // true

// js is beautiful!
console.log(8 * null); // 0
console.log(typeof (8 * null)); // number
console.log("5" - 1); // 4
console.log(typeof ("5" - 1)); // number
console.log("5" + 1); // 51
console.log(typeof ("5" + 1)); // string
console.log("five" * 2); //  NaN
console.log(typeof ("five" * 2)); // number
console.log(false == 0); // true
console.log(typeof (false == 0)); // boolean
console.log(null == undefined); // true
console.log(null == 0); // false
console.log(0 == false); // true
console.log("" == false); // true
console.log("" === false); // false

// prompt("Enter passcode");

// ex-0 looping triangle
let hash = "#";
for (let i = 0; i < 7; i++) {
    console.log(hash);
    hash += "#";
}
// ex-1 FizzBuzz
let result = null;
for (let i = 0; i < 100; i++) {
    if (i % 3 == 0 && i % 5 != 0) {
        result = "Fizz";
    } else if (i % 5 == 0 && i % 3 != 0) {
        result = "Buzz";
    } else {
        result = i;  
    }
    console.log(result);
}

result = null;
for (let i = 0; i < 100; i++) {
    if (i % 3 == 0 && i % 5 != 0) {
        result = "Fizz";
    } else if (i % 5 == 0 && i % 3 != 0) {
        result = "Buzz";
    } else if (i % 3 == 0 && i % 5 == 0){
        result = "FizzBuzz";
    } else {
        result = i;
    }
    console.log(result);
}

// ex-03 chess board
let strStart = " # # # #";
const length = strStart.length;
for (let i=0; i<8; i++){
    console.log(strStart);
    strStart = strStart.slice(1, length) + strStart[0];    
}
