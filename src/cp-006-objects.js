function speak(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
}
let whiteRabbit = { type: "white", speak };
let hungryRabbit = { type: "hungry", speak };
whiteRabbit.speak("Hi");
speak.call(hungryRabbit, "Burp!");

function normalize() {
  console.log(this.coords.map((n) => n / this.length));
}
let data = { coords: [0, 2, 3], length: 5, normalize };
data.normalize();

function normalize1(array) {
  console.log(this);
  console.log("-----");
  array.map((n) => console.log(this));
}

normalize1([2, 3, 4]);

let protoRabbit = {
  speak(line) {
    console.log(`The ${this.type} rabbit says '${line}'`);
  },
};
let killerRabbit = Object.create(protoRabbit);
killerRabbit.type = "killer";
killerRabbit.speak("SKREEEE!");

function Rabbit(type) {
  this.type = type;
}
Rabbit.prototype.speak = function (line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
};
let weirdRabbit = new Rabbit("weird");
weirdRabbit.speak("Deliveroo??");
