function every(array, predicate) {
  for (let element of array) {
    if (!predicate(element)) {
      return false;
    }
  }
  return true;
}

function every2(array, predicate) {
  return !array.some((element) => !predicate(element));
}

console.log([3, 4, 5].some((n) => n < 5));
console.log([3, 4, 5].every((n) => n < 6));
console.log("-----");
console.log(every([(3, 4, 5)], (n) => n < 6));
console.log(every([(3, 4, 5)], (n) => n < 4));
console.log("-----");
console.log(every2([(3, 4, 5)], (n) => n < 6));
console.log(every2([(3, 4, 5)], (n) => n < 4));
