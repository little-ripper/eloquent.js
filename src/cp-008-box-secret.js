const box = {
  locked: true,
  unlock() {
    this.locked = false;
  },
  lock() {
    this.locked = true;
  },
  _content: [],
  get content() {
    if (this.locked) throw new Error("Locked!");
    return this._content;
  },
};

function withBoxUnlocked(func) {
  let startState = true;
  if (!box.locked) {
    startState = box.locked;
  } else {
    box.unlock();
  }
  console.log(`Box locked: ${box.locked} -> unlocked`);
  try {
    func();
  } catch (e) {
    throw new Error(e);
  } finally {
    if (!startState) {
      console.log(`Box was unlocked: ${box.locked} -> unlocked`);
      return;
    }
    box.lock();
    console.log(`Box locked: ${box.locked} -> locked`);
    return;
  }
}

withBoxUnlocked(() => console.log("I AM RUNNING"));
box.locked = false;
withBoxUnlocked(() => console.log("I AM RUNNING"));
