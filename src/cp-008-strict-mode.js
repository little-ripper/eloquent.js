("use strict");

function canYouSpotTheProblem() {
  "use strict";
  // try to remove let and see the message in 2 cases:
  // - "use strict"
  // - without "use strict"
  for (let counter = 0; counter < 10; counter++) {
    console.log("HAPPINO..");
  }
}

canYouSpotTheProblem();
function Person(name) {
  this.name = name;
}
let ferdinand = Person("Ferdinand");
console.log(name); // Ferdinand
console.log(ferdinand); // undefined

// test
function test(label, body) {
  if (!body()) console.log(`Failed: ${label}`);
  console.log("PASS..");
}
test("convert Latin text to uppercase", () => {
  return "hello".toUpperCase() == "HELLO";
});
test("convert Greek text to uppercase", () => {
  return "Χαίρετε".toUpperCase() == "ΧΑΊΡΕΤΕ";
});
test("don't convert case-less characters", () => {
  return "߈࠶ࢎఆ௷".toUpperCase() == "߈࠶ࢎఆ௷";
});
