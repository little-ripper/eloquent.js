/* Your code here.
 *
 * let obj = {here: {is: "an"}, object: 2};
 * console.log(deepEqual(obj, obj));
 * // → true
 * console.log(deepEqual(obj, {here: 1, object: 2}));
 * // → false
 * console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
 * // → true */

const obj0 = { val0: { val00: 0 }, val1: 1 };
const obj1 = { val0: { val00: 1 }, val1: 1 };
const obj2 = { val0: { val00: 121 }, val1: 1 };
const obj3 = { val0: { val00: 0 }, val1: 1 };

function deepEqual(obj0, obj1) {
  let obj0keys = Object.keys(obj0);
  let obj1keys = Object.keys(obj1);

  for (let key of obj0keys) {
    if (obj0[key] === obj1[key]) {
      return true;
    } else if (typeof obj0[key] === "object" && typeof obj1[key] === "object") {
      return deepEqual(obj0[key], obj1[key]);
    } else {
      return false;
    }
  }
}
let obj = { here: { is: "an" }, object: 2 };
console.log(deepEqual(obj, obj));
console.log(deepEqual(obj, { here: 1, object: 2 }));
console.log(deepEqual(obj, { here: { is: "an" }, object: 2 }));
console.log("-----");
console.log(deepEqual(obj0, obj1));
console.log(deepEqual(obj0, obj2));
console.log(deepEqual(obj0, obj3));
