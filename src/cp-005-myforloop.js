function loop(value, test, update, body) {
  /* if (!test(value)) {
   *   return value;
   * }
   * update(body(value)) */
  for (let i = value; test(i); i = update(i)) {
    body(i);
  }
}

loop(
  0,
  (n) => n <= 3,
  (n) => n + 1,
  console.log,
);
