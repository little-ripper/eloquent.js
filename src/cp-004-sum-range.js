function range(start, end, step) {
  let result = [];
  step = step ? step : 1;
  if (step < 0) {
    if (end > start) {
      return [];
    }
    for (let i = start; i >= end; i = i + step) {
      result.push(i);
    }
  }
  for (let i = start; i <= end; i = i + step) {
    result.push(i);
  }
  return result;
}
console.log(range(1, 10));
console.log(range(1, 10, 2));
console.log(range(10, 1, -2));
console.log(range(1, 10, -4));

function sum(arr) {
  let result = 0;
  for (let number of arr) {
    result += number;
  }
  return result;
}

console.log(sum(range(1, 10)));
