class Group {
  constructor() {
    this.group = [];
  }

  add(value) {
    if (!this.has(value)) {
      this.group.push(value);
    }
  }

  del(value) {
    if (this.has(value)) {
      this.group = this.group.slice(0, indx).concat(this.group.slice(indx + 1));
    }
  }

  has(value) {
    const indx = this.group.indexOf(value);
    if (indx === -1) {
      return false;
    }
    return true;
  }

  static fromGroup(array) {
    let g = new Group();
    for (let element of array) {
      if (!g.has(element)) g.add(element);
    }
    return g;
  }

  [Symbol.iterator]() {
    return new GroupIterator(this);
  }
}

class GroupIterator {
  constructor(g) {
    this.group = g.group;
    this.counter = 0;
  }

  next() {
    if (this.group.length == this.counter) return { done: true };
    let value = this.group[this.counter];
    this.counter++;
    return { value, done: false };
  }
}
/*
 * Group.prototype[Symbol.iterator] = function () {
 *   return new GroupIterator(this);
 * };
 *  */
g0 = new Group();
g0.add(3);
g0.add(3);
g0.add(2);
g0.add(1);
console.log(g0);

g1 = Group.fromGroup([1, 3, 3, 4, 5, 6, 7, 8, 9, 9, 7]);
console.log(g1);

for (let g of g1) {
  console.log(`Elements for the group: g1 -> ${g}`);
}
