const toString = Symbol("toString");
Array.prototype[toString] = function () {
  return `${this.length} cm of blue yarn`;
};
console.log([1, 2].toString());
// → 1,2
console.log([1, 2][toString]());
console.log([1, 2][toString].call([1, 2]));
